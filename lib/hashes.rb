# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def ele_count(arr)
  ele_count = Hash.new(0)
  arr.each{|word| ele_count[word] = word.length}
  ele_count
end

def word_lengths(str)
  ele_count(str.split())
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by{ |k,v| v }.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each{|key, value| older[key] = value}
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  count = Hash.new(0)
  word.each_char{|char| count[char] += 1}
  count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  result = {}
  arr.each{|ele| result[ele] = true}
  result.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
   even_odd_counter = Hash.new(0)
    numbers.each do |number|
     number.even? ? even_odd_counter[:even] += 1 : even_odd_counter[:odd] += 1
    end
    even_odd_counter
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  count = Hash.new(0)
  string.each_char {|char| count[char] += 1 if vowels.include?(char)}
  result = "a"
  count.each do |key, value|
    result = key if key > result && value > count[result]
  end
  result
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  names = students.select{|stu, birthday| birthday >= 7 }.keys
  result = []
  names.each_index do |index|
     names.each_index do |index2|
       next unless index2 > index
       result << [names[index], names[index2]]
     end
  end
  result
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  number_of_species = specimens.uniq.size
  specimens_count = Hash.new(0)
  specimens.each { |specie| specimens_count[specie] += 1 }
  smallest_population = specimens_count.values.min
  largest_population = specimens_count.values.max
  number_of_species**2 * smallest_population / largest_population
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true

def can_tweak_sign?(normal_sign, vandalized_sign)
   normal_sign_count = character_count(normal_sign)
   vandalized_sign_count = character_count(vandalized_sign)
   vandalized_sign_count.all? do |key, value|
     normal_sign_count[key] >= value
   end

end

def character_count(str)
  alphabet = ("a".."z").to_a
  char_count = Hash.new(0)
  str.each_char{ |char| char_count[char] += 1 if alphabet.include?(char) }
  char_count
end
